# Software Testing and Quality Assurance 2022

- 3rd period; spring semester
- Passed with 5/5

An insightful course into both theory and practice of software testing and QA. Myself, I mainly appreciated the theory, manual testing know-how and the opportunity to compile a quality model for a fictional company.

## Quick Summary

During lectures different testing techniques, definitions in the software testing area and other theory was talked about and after each a student had to write a brief learning diary that was every week submitted.

Solo projects were a combination of writing unit tests, acceptance tests, automation pipelines and carrying out scripted manual testing sessions.

Group project had the ultimate goal of compiling a quality model for a fictional company. The background "story" provided the history of the company, planned growth, stakeholders and several use cases.
