# CS-E4960 Learning diary 05

# 1. Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

- group work feedback

- dive into software quality control (SQC)
    = concrete activities in the QA process
- functional suitability: completeness (all is done), correctness (all is correct), appropriateness (all is correct according to context)
- SUT = System Under Test
- difference between black-box & white-box testing
    - they are only test techniques
- introduction of test oracle (see question 3 for more info)
- black-box testing
    - all levels of testing; exploratory testing

- BVA = Booundary value analysis
    - useful for comparisons, loop termination conditions
    - robustness testing: testing even on invalid data
    - lowers the number of test cases
- Decision table testing
    - list all inputs and all actions arising from them
- State transition testing
    - state = a "stable" system condition
    - transition = a trigger moving the system from one state to another
    - table based with states, events and transitions -> basis for test cases

# 2. What is the difference between black-box and white-box testing, and when would you use which?

In black-box testing the inner working of the tested system are not known while in white-box testing they are known.

The usage of a technique depends on the context. Do we want to test the behaviour of the system (or its part) by passing an input and checking the outcome? Use black-box testing. Do we want to test the system by checking its internals? Use white-box testing.

# 3. What is a test oracle, what kinds of test oracles are there (mention four kinds), and how is the concept of a test oracle relevant for software testing?

"A mechanism for determining whether a test has passed or failed"

The definition could be simplified into "source of truth for a test".

Types: specified, derived, implicit, human

Each test produces some kind of a result that has to be interpreted in a certain way. Depending on the interpretation the result will be deemed as a success or as a failure. A testing oracle can serve as the interpret of the test results.
