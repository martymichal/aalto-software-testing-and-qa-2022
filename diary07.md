# CS-E4960 Learning diary 07

# Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

The lecture started with some practicalities followed by sharing experiences from the exploratory testing assignment.

The informational part started with a brief summary of black-box testing and more in-depth look at white-box testing. White-box testing can be split into two parts: static testing and dynamic testing.

Dynamic testing can be approach from many angles. The goal can be to achieve statement coverage, test paths, decision/condition coverage, control flow or data flow. They are classified into several levels: unit, integration, system, regression.

Test coverage metrics were the next topic where the first example was statement coverage, followed by path coverage.

Path coverage classifies the behavior of a program into a set of distinct paths (could be interpreted as a finite state machine) and shows the testing coverage of these different paths (states). The formulae is 'total paths executed / total paths in program'.

Decision/Condition coverage covers all decision/conditions in a project. The formulae is 'total decisions executed / total decisions in program'. All combinations of conditionals have to be covered.

The next topics were product metrics and monitoring progress in a project accompanied by a brainstorming on the topic whether a project can be released based on results from 4 previous sprints with information on the number of defects and the number of planned + executed tests.

Monitoring of progress is an important activity helping in future planning leading to more effective development.

The covered topics were quite insightful. I mainly appreciated the in-depth view of white-box testing and product metrics coupled with the brainstorming exercise. I don't have any questions for this lecture.

# What is static white-box testing? Give two examples of static white-box testing techniques and shortly explain when you would use them.

Type of white-box testing that serves mainly for preventing defects creeping into a project through changes. The testing strives to be as minimal as possible as it can be used very actively as it is used in code reviews or inspections. The minimalism can be achieved by identifying the most critical parts of a project and testing them. Selective testing could also be in-place (meaning that only affected parts + critical parts are tested when a change is introduced).

# What is the purpose of test coverage metrics? Give an example of a test coverage metric and explain what it tells.

Give an overview of different aspects of a project related to reliability, testability and maintainability. They help in planning what test need to be implemented to cover as much of a project as possible.

Example: Statement coverage

It is a ratio of 'tested statements / number of statements in project'. It signifies that a certain ratio of statements in a project behaves predictably.

Not perfectly easy to cover completely as many statements can be hidden behind 'early returns', large number of conditionals is in place or many loops are in place, making it challenging to check boundary conditions.
