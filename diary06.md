# CS-E4960 Learning diary 06

# 1. Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

# 2. When would you choose manual testing over automated testing? Motivate your answer, not only by an example, but through the inherent characteristics of manual testing.

# 3. How can boundary value analysis be of use in exploratory testing? Give at least one example.
