# CS-E4960 Learning diary 03

# 1. Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

- Learning diary feedback
- User stories are (usually) short texts that are translated to more clear acceptance criteria
    - They are written from a user's perspective
- Connecting high-level concerns/requirements with low-level definitions of situations
- Introduction of terms: Continuous integration (CI), continuous delivery (CI+), continuous deployment (CD)
    - Continuous Integration is mainly for running tests to verify the sw
    - Continuous Delivery ensures a sw is ready for release
    - Continuous Deployment creates a release (very loose term; many tasks can be connected to a release) if a sw passes all tests
- Introduction of test types: unit tests, acceptance tests, performance tests, load tests,...
- Led a discussion on what tests are (not) suitable for automation

I have been already familiar with the concept of user stories, CI, CI+, CD and various types of tests. The lecture still helped me to more clearly define theses terms and the discussion extended my knowledge of different types of tests.

The idea of automated tests has had me thinking for quite some time about the ecological impact of this feature. Constant usage that is often non-trivial is quite demanding.

# 2. What is verification and validation in software quality assurance?

## Verification

- Technical view of the software's quality. Are used only good practices? Does the software follow given specifications? Is the software buggy (errors, faults)?

## Validation

- Practical view of the software's quality. Is the user satisfied? Are given requirements satisfied?

# 3. What benefits could test automation bring to the software testing process, and what limitations are there to test automation? (According to Mistrik and the lecture.)

Benefits consist of instant validation of features and prevention of regressions. Complicated test cases can be difficult to program and even use of test generation software often leaves some parts of a codebase uncovered.
