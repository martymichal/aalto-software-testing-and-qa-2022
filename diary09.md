# CS-E4960 Learning diary 09

# 1. Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

The lecture started by explaining different metrics for software quality. There 3 main types: product metrics, process metrics and project metrics. The lecture mainly focused on product metrics. A brainstorming session followed about what kind of metrics can be gathered in a software project. Among the presented metrics were: lines of code (LoC), functions, classes, cyclomatic complexity, database size, number of files, dependencies, mean time to failure, test coverage, customer satisfaction, uptime/downtime or number of support tickets. The brainstorming was followed by monitoring of progress of a software test plan. Based on this a testing team can prioritize testing efforts. Test plans were the next topic. They help with the planning of testing of a software project. Different factors influence the creation of such a plan. Test Progress S Curve is explained in point 3. Next topic was Test-Driven Development (TDD) where an endless cycle of /: "write test" -> "write code so the test passes" -> "refactor" :/. Mutation testing followed. It is a white-box testing technique that creates mutations of the tested software that contain artificially inserted faults. If a faulty mutant passes the test-suite, the suite needs to be updated. The problematic part is the introduction of mutations.

I've already been familiar with most of the different testing metrics, TDD and mutation testing. A new piece of information was Test Progress S Curve which is a bit difficult for me to imagine how to apply it in practice since a number of tests is not a completely correct indicator of coverage.

# 2. What is the difference between software product quality metrics and software process quality metrics? Give at least one example of each.

Software project quality metrics are tied to the fact how a project is run. They are not tied to the actual implementation of tests but rather the operational part of their making.

Software product quality metrics "describe the characteristics of the product". They can get very low-level and show the current health of the product.

# 3. How can the Test Progress S Curve help to answer the question: Can we release our software?

The curve uses a concept of "test points" (an abstract metric that can be adjusted) on the y axis with time on the x axis. Points on the curve are the test plan (number of points in a certain point of time) and throughout its completion it is being filled with information about the number of attempted and completed tests. The curve helps to monitor to fulfillment of the testing plan: have we created enough tests? have we completed enough tests? The curve can also be filled with the number of defects found at a certain point of time. The curve helps to project the anticipated volume of defects if a release is made at a certain point of time. If the volume is deemed small enough and the number of incoming defect reports is fine a release could be made.
