# CS-E4960 Learning diary 04

# 1. Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

- group project practicality
- life cycle of software - in the past stages in series, nowadays more parallelized; better flexibility of development
- definition of test automation
- What to automate? - as many monotonous and boring tasks
- defining the terminology
    - core questions: how?, why?, on what level?
    - how? - black-box, white-box, grey-box (knowledge of the internals)
    - why? - regression, exploratory
    - on what level? - unit, component, integration, system, acceptance, sth extra
- discussion regarding the level of testing (classify 2 example tests)
- walk through a modern development pipeline
- user stories
    - basic structure: When (situation), I want to (motivation), So I can (Expected Outcome)
- acceptance-test driven development (ATDD)
- virtualization
    - all test environments should be virtualized (at least the automated ones)
- maintenance
    - tests for stabilised features shift to regressions side of the testing spectrum
    - all fixed bugs should have a test case
- summary of benefits of test automation
- example of test automation in GE Healthcare
- walk through a generic QA tech stack

I already knew the basic testing terminology and general knowledge. The view of a modern development pipeline confirmed my experience with open-source software. User stories were a known concept to me but I never used the concept during development. ATDD is an interesting concept that I wasn't exactly aware of that nicely encapsulates TDD.

The lecture was on point, insightful and did not leave any questions.

# 2. The guest lecturer described a conceptual model of how testing and QA works in modern software development with both manual and automatic tests. This model includes user stories and acceptance-test driven development (ATDD). How do user stories and ATDD work together in the model to ensure quality and inform stakeholders of the quality status of the software?

Stakeholders may make a feature request (or it may not even come from direct stakeholders). The feature is broke down into user stories to have a concrete example of what the feature is for and therefore how it can be tested. Once all user stories have been covered by tests and the tests are passing, the feature has been implemented and users can be notified of its availability.

# 3. What insights did the guest lecturer present regarding exploratory tests in maintenance? Explain the motivations behind these insights.

Exploratory tests are created during the development phase of software development. During maintenance a part of them becomes regression tests and join the regression test spectrum of testing to ensure the feature they're covering does not regress. Adding all exploratory tests to the regressions test set could result in duplication and could lead the testing routine to be too long without much of a benefit.
