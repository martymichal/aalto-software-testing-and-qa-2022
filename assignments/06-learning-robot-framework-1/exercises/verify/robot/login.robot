*** Settings ***
Library    SeleniumLibrary

*** Variables ***

${URL}    http://localhost:7272
${DEMO_USER_NAME}    demo
${DEMO_USER_PASS}    mode

*** Keywords ***

Open Browser To Login Page
    Open Browser    ${URL}    browser=gc

Login User
    [Arguments]    ${name}    ${password}
    Input Text    id=username_field    ${name}
    Input Text    id=password_field    ${password}
    Submit Form    name=login_form

Verify That Welcome Page Is Visible
    Location Should Contain    ${URL}/welcome.html
    Page Should Contain    Welcome Page
    Title Should Be    Welcome Page

*** Test Cases ***

Welcome Page Should Be Visible After Successful Login
    Open Browser To Login Page
    Login User    ${DEMO_USER_NAME}    ${DEMO_USER_PASS}
    Verify That Welcome Page Is Visible
    Close Browser