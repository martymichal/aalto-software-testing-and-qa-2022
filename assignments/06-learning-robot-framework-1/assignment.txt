In the previous assignments you have got the virtual machine running, made some unit tests with Python’s unittest framework and analyzed these tests. One purpose of these assignments was to get you familiar with the project and the testing environment. Another purpose was to get you thinking about how to accomplish tests using contemporary tools.

Another tool we will focus on this course is Robot Framework, which is especially useful for creating automated acceptance tests. You have probably already seen and executed the Robot tests present in the HelloWeb project, but now we will learn how to make these tests.

For this assignment you will complete exercises from Eficode’s Robot Framework tutorial (see below).

To complete this assignment, do the following:

    Download the project to your virtual machine from https://github.com/eficode-academy/rf-katas.
    Complete the steps 0-3, found in the exercises folder.
        In the first step you only need to install robotframework-lint and check that the project starts correctly.
        In the second step the instructions tell you to make the robot folder in the root directory, but for the verifications to work it has to be actually made to the exercises/verify folder.
        Don’t worry too much about the results after the first 2 steps as they won’t need to be submitted, only the final result in step 3 will be submitted.
        If you want to run the provided verifications and the linting does not pass because it can’t find the setup .args file, you can in this case skip the linting by commenting out the “run_linting()” function in the corresponding setup python file.
    Submit the completed project folder to the submission field below.

(Note: RobotFramework expects TAB characters in the robot files as separators, not spaces! This is a common mistake, so if you get strange errors, make sure you have TABs in the right places!)
