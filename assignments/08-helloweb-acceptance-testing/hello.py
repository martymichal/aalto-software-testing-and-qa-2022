from random import choice
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer

NAME_DB_FILE = "names.txt" # File containing names, one per line
CONVERSATION_DB_FILE = "conversation.txt" # File containing things to say, one per line

class Hello:
    
    def __init__(self, name = "World", names_file = NAME_DB_FILE, conversation_file = CONVERSATION_DB_FILE, chatbot_enabled = True):
        self.name = name
        self.file = names_file
        with open(names_file) as namedb:
            self.__name_list = [name.strip() for name in namedb.readlines()]
        
        if chatbot_enabled:
            self.__chatbot = ChatBot("Greeter")
            trainer = ListTrainer(self.__chatbot)
            conversation = []
            with open(conversation_file) as conversationdb:
                conversation = [line.strip() for line in conversationdb.readlines()]
            trainer.train(conversation)

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = str(value)
    
    @property
    def name_list(self):
        return self.__name_list

    def add_name(self, name):
        self.__name_list.append(name)

    def set_random_name(self):
        self.name = choice(self.__name_list)

    def check_name(self, name):
        if name in self.__name_list:
            return 'Name found in name list'
        else:
            return 'No such name in name list'

    def remove_name(self, name):
        self.__name_list.remove(name)

    def save(self):
        with open(self.file, "w") as namedb:
            for name in self.__name_list:
                namedb.write("{}\n".format(name))

    def get_greeting(self):
        if len(self.name) == 0:
            return "Hello!"
        else:
            return "Hello, {}!".format(self.name)

    def get_response(self, statement):
        return self.__chatbot.get_response(statement)

    def __str__(self):
        return "<Hello:{} names>".format(len(self.__name_list))

