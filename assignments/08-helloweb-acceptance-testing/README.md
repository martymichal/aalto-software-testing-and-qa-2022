# HelloWeb version 1

Template for basic test environment.

## What you can do

**Play with the `Hello` class**

```
$ python -i hello.py
>>> hello = Hello()
>>> print(hello)
Hello, World!
>>> hello.name = "Yourname"
>>> print(hello)
Hello, Yourname!
```

**Run the unit test**

```
$ python tests.py
..
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

**Run the Robot test**

First start the HelloWeb application. Use the Git Bash terminal for this command.
Git Bash terminal can be started by right clicking the project folder and selecting "Git Bash here"

```
$ FLASK_APP=helloweb.py FLASK_ENV=development flask run
```

You can now access the web application at the address provided by Flask (usually `http://127.0.0.1:5000/`).

Then run the Robot test

```
$ robot browse.robot
==============================================================================
Browse
==============================================================================
Site Language Is Set By URL                                           | PASS |
------------------------------------------------------------------------------
Browse                                                                | PASS |
1 critical test, 1 passed, 0 failed
1 test total, 1 passed, 0 failed
==============================================================================
```

You can find the detailed test results in the files `output.xml`, `log.html`, and `report.html`.
