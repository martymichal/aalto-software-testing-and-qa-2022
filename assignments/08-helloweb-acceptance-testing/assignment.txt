For the last technical assignment, you will create some acceptance tests based on user stories for the same HelloWeb application. The detailed instructions are provided in the PDF file.

The assignment folder is again provided below. Submit the completed assignment folder as a zip file to the submission field. Set the folder name as "HelloWeb_6_studentname".
