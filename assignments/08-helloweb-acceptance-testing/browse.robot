*** Settings ***
Library    SeleniumLibrary
Test Setup    Open Main Page
Test Teardown    Reset And Close Browser

*** Variables ***
${URL}    http://127.0.0.1:5000
${BROWSER}    Chrome

*** Test Cases ***
Main Page Greeting
    Check Main Page Greeting

Greeting With A Specific Name
    Submit Name    Robot
    Check Named Greeting    Robot

Greeting With A Random Name
    Click No Name Button
    Check Any Name Greeting

Conversation Works
    Submit Name    Robot
    Submit Generic Statement
    Check For Response

Chat With Bot Twice With Different Names
    Submit Name    Robot1
    Check Named Greeting    Robot1
    Submit Statement    I'm the first one to talk!
    Click Quit Button
    Submit Name    Robot2
    Check Named Greeting    Robot2
    Submit Statement    I'm the second one to talk!

Exchange With Bot Several Messages
    Click No Name Button
    Check Any Name Greeting
    Submit Generic Statement
    Submit Generic Statement
    Submit Generic Statement
    Submit Generic Statement
    Submit Generic Statement
    Page Should Contain Element    css:div#conversation p    limit=11

*** Keywords ***
Open Main Page
    Open Browser    ${URL}    ${BROWSER}

Reset And Close Browser
    Go To    ${URL}/reset
    Close Browser

Submit Name
    [Arguments]    ${name}
    Input Text    id=name   ${name}
    Click Button    id=hello

Submit Generic Statement
    Submit Statement    Is this working?

Submit Statement
    [Arguments]    ${statement}
    Input Text    id=statement    ${statement}
    Click Button    id=hello

Check Main Page Greeting
    Title Should Be    Hello Web
    Element Text Should Be    id=greeting    Hi! What's your name?

Check Named Greeting
    [Arguments]    ${name}
    Page Should Contain    Hello, ${name}!

Click No Name Button
    Click Button    id=no_name

Click Quit Button
    Click Button    id=reset

Check Any Name Greeting
    ${greeting}=    Get Text    xpath=//*[@id='conversation']/p
    Should Match Regexp    ${greeting}    Hello, \\w+!

Check For Response
    Page Should Contain Element    class:bot    limit=2