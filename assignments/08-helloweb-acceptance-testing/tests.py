# Basic template for Python unit tests
# NOTE: Use Python 3

# Import the unittest module to use the Python unit test framework
import unittest

# Import the modules and/or classes that are being tested
from hello import Hello


TEST_FILE = "names_test.txt" # File for testing the names

TEST_NAMES = [
    "Florentina",
    "Adina",
    "Loreen",
    "Yolanda",
    "Reynaldo",
    "Keri",
]

# A test case consists of individual steps, represented as methods.
# These methods MUST start with "test". Some special methods exist
# to initialise and finalise the test case run.


class HelloClassTestCase(unittest.TestCase):
    def setUp(self):
        # Reset the name list file before every test
        with open(TEST_FILE, "w") as namedb:
            for name in TEST_NAMES:
                namedb.write("{}\n".format(name))
        self.hello = Hello(names_file = TEST_FILE, chatbot_enabled=False)

    def testGreeting(self):
        """Check that the default hello world string is present."""
        self.assertEqual(self.hello.get_greeting(), "Hello, World!")

    def testCustomGreeting(self):
        """Check that it greets with a custom name."""
        self.hello.name = "Custom"
        self.assertEqual(self.hello.get_greeting(), "Hello, Custom!")

    def testNonameGreeting(self):
        """Check that the hello world defaults to "Hello!" when using an empty string"""
        self.hello.name = ""
        self.assertEqual(self.hello.get_greeting(), "Hello!")
    
    def testAdd(self):
        """Check that the add method adds a name to the name list correctly"""
        self.assertEqual(len(self.hello.name_list), len(TEST_NAMES))
        self.hello.add_name("Testname")
        self.assertIn("Testname", self.hello.name_list)
        self.assertEqual(len(self.hello.name_list), len(TEST_NAMES) + 1)
    
    def testRandomName(self):
        """Should set a random name from the name list as the name"""
        self.hello.set_random_name()
        self.assertIn(self.hello.name, self.hello.name_list)

    def testRemoveName(self):
        """Should remove the name from the list"""
        self.hello.add_name("Testname")
        self.hello.remove_name("Testname")
        self.assertNotIn("Testname", self.hello.name_list)
        self.assertEqual(len(self.hello.name_list), len(TEST_NAMES))

    def testCheckName(self):
        """Should check that the name is found in the list"""
        self.assertEqual(self.hello.check_name("Testname"), "No such name in name list")
        self.hello.add_name("Testname")
        self.assertEqual(self.hello.check_name("Testname"), "Name found in name list")


    def testSave(self):
        """Should save name_list to the file names.txt"""
        self.hello.add_name("Testname")
        self.hello.save()
        new_name_list = []
        with open(TEST_FILE) as namedb:
            new_name_list = [name.strip() for name in namedb.readlines()]
        self.assertIn("Testname", new_name_list)

    def testGetResponse(self):
        """Chatbot should send a response"""
        self.hello = Hello(names_file = TEST_FILE, chatbot_enabled=True)
        self.assertIsInstance(self.hello.get_response("Test").text, str)


# This is what gets run when Python is started on this file.
if __name__ == "__main__":
    # The unittest framework will discover all test cases in the file
    # and run the test methods in them.
    unittest.main()
