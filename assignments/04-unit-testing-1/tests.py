# Basic template for Python unit tests
# NOTE: Use Python 3

# Import the unittest module to use the Python unit test framework
from os import remove
import unittest

# Import the modules and/or classes that are being tested
from hello import Hello

# A test case consists of individual steps, represented as methods.
# These methods MUST start with "test". Some special methods exist
# to initialise and finalise the test case run.

# File for testing the names
TEST_FILE = "names_test.txt"
TEST_FILE_EMPTY = "names_test_empty.txt"

TEST_NAMES = [
    "Florentina",
    "Adina",
    "Loreen",
    "Yolanda",
    "Reynaldo",
    "Keri",
]


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        self.hello = Hello(file=TEST_FILE)

    def testHelloString(self):
        """Check that the default hello world string is present."""
        self.assertEqual(str(self.hello), "Hello, World!")

    def testChangeHelloString(self):
        """Check that the hello world string changes when the name is changed."""
        self.hello.name = "Planet"
        self.assertEqual(str(self.hello), "Hello, Planet!")

    def testEmptyName(self):
        """Check that the hello world defaults to "Hello!" when using an empty string"""
        self.hello.name = ""
        self.assertEqual(str(self.hello), "Hello!")

class AddNameTests(unittest.TestCase):
    def setUp(self):
        self.hello = Hello(file=TEST_FILE)

    def testAddEmptyName(self):
        self.hello.add_name("")
        self.assertEqual(len(self.hello._Hello__name_list), 8)
        self.assertEqual(self.hello._Hello__name_list[-1], "")
        self.assertEqual(str(self.hello.name), "World")

    def testAddSingleName(self):
        self.hello.add_name("Margaret")
        self.assertEqual(len(self.hello._Hello__name_list), 8)
        self.assertEqual(self.hello._Hello__name_list[-1], "Margaret")
        self.assertEqual(str(self.hello.name), "World")

class RemoveNameTests(unittest.TestCase):
    def setUp(self):
        self.hello = Hello(file=TEST_FILE)

    def testRemoveOneName(self):
        self.hello.remove_name("Florentina")
        self.assertEqual(len(self.hello._Hello__name_list), 6)
        self.assertEqual(str(self.hello.name), "World")
        self.assertFalse("Florentina" in self.hello._Hello__name_list)

    def testRemoveCurrentlySelectedName(self):
        self.hello.remove_name("World")
        self.assertEqual(len(self.hello._Hello__name_list), 6)
        self.assertEqual(str(self.hello.name), "Florentina")
        self.assertFalse("World" in self.hello._Hello__name_list)

class CheckNameTests(unittest.TestCase):
    def setUp(self):
        self.hello = Hello(file=TEST_FILE)

    def testNameExists(self):
        self.assertEqual(self.hello.check_name("World"), 'Name found in name list')
        self.assertEqual(self.hello.check_name("Florentina"), 'Name found in name list')
        self.hello.add_name("Foo Bar")
        self.assertEqual(self.hello.check_name("Foo Bar"), 'Name found in name list')


    def testNameNotExists(self):
        self.assertEqual(self.hello.check_name("Foo Bar"), 'No such name in name list')
        self.assertEqual(self.hello.check_name("WorldWideWeb"), 'No such name in name list')

class SetRandomNameTests(unittest.TestCase):
    def setUp(self):
        self.hello = Hello(file=TEST_FILE)
        self.name_list = ["World"]
        self.name_list.extend(TEST_NAMES)

    def testDefaultStateRandom(self):
        for _ in range(10):
            self.hello.set_random_name()
            self.assertTrue(self.hello.name in self.name_list)

    def testDefaultStateControlled(self):
        self.hello.name = "FooBar"
        self.assertEqual(self.hello.name, "FooBar")
        self.hello.set_random_name()
        self.assertNotEqual(self.hello.name, "FooBar")
        self.assertTrue(self.hello.name in self.name_list)

class SaveTests(unittest.TestCase):
    def setUp(self):
        self.full = Hello(file=TEST_FILE)
        self.full.file = TEST_FILE_EMPTY
        self.empty = Hello(file=TEST_FILE_EMPTY)

    def tearDown(self):
        try:
            remove(TEST_FILE_EMPTY)
        except:
            pass

    def testDefaultStateSave(self):
        self.full.save()
        with open(TEST_FILE_EMPTY, "r") as f:
            self.assertEqual(f.readline().strip(), 'World')
            for name in TEST_NAMES:
                self.assertEqual(f.readline().strip(), name)

    def testFromEmptyState(self):
        for name in TEST_NAMES:
            self.empty.add_name(name)
        self.empty.save()
        with open(TEST_FILE_EMPTY, "r") as f:
            self.assertEqual(f.readline().strip(), 'World')
            for name in TEST_NAMES:
                self.assertEqual(f.readline().strip(), name)

# This is what gets run when Python is started on this file.
if __name__ == "__main__":
    # The unittest framework will discover all test cases in the file
    # and run the test methods in them.
    unittest.main()
