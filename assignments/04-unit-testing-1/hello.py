from random import choice

NAME_DB_FILE = "names.txt" # File containing names, one per line

class Hello:
    """A simple class for generating a hello world string."""
    
    def __init__(self, name = "World", file = NAME_DB_FILE):
        self.file = file
        self.__name = name
        self.__name_list = [name]
        try:
            with open(self.file) as namedb:
                self.__name_list.extend(name.strip() for name in namedb.readlines())
        except:
            pass


    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = str(value)

    def add_name(self, name):
        self.__name_list.append(name)

    def remove_name(self, name):
        self.__name_list.remove(name)
        if name == self.name:
            self.name = self.__name_list[0]

    def set_random_name(self):
        self.__name = choice(self.__name_list)

    def check_name(self, name):
        if name in self.__name_list:
            return 'Name found in name list'
        else:
            return 'No such name in name list'

    def save(self):
        with open(self.file, "w") as namedb:
            for name in self.__name_list:
                namedb.write("{}\n".format(name))


    def __str__(self):
        if len(self.name) == 0:
            return "Hello!"
        else:
            return "Hello, {}!".format(self.name)
