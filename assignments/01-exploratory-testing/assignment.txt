Exploratory testing is a systematic approach to manual testing that does not rely on strictly scripted test cases. The purpose of this task is to learn about two approaches to exploratory testing through applying them, and to form an advanced understanding of exploratory testing, including how it can be planned and performed, what choices the tester faces, and how test results can be recorded and reported.

In this task, you must:

    Perform and report on a small exploratory test using session-based test management.
    Perform and report on a small exploratory test using tour-based exploratory testing.
    Make a comparative analysis of the two approaches.

The application to test is the Saleor e-commerce platform's storefront interface. You should perform the test in the virtual machine that has been provided to you. The virtual machine has Saleor pre-installed and you can access it by opening a browser in the virtual machine and opening http://localhost:3000/. To access the virtual machine, please refer to the email you have received and the instructions here: https://docs.microsoft.com/en-us/azure/lab-services/tutorial-connect-virtual-machine-classroom-lab

Note: The virtual machines have a specific version of Saleor installed, to ensure a well-defined starting point for everyone's exploration. You should use the virtual machine for this assignment and not install Saleor on your own machine for the assignment. Start the Saleor software by typing the following in a command prompt:

docker-compose up

Starting the application can take quite a while so be prepared to wait a bit before you access it.

Please read the instructions below carefully from start to finish before you begin this assignment.
Step 1: Tour-based exploratory testing

    Get familiar with tour-based exploratory testing through the lecture materials and by skimming Chapter 4 of Whittaker (2009). In Whittaker (2009), focus especially on understanding the concept of Districts and the Tours described. Think about how they would direct exploratory testing.
    Prepare for the test tour by:
        Copying the provided template into a text editor. (tour-report-template.txt).
        Choosing an appropriate tour from Chapter 4 in Whittaker (2009). Fill in the tour name and write a very short summary of the purpose of the tour.
        Elaborate the intent of the tour. Fill in the intent of your exploration.
        The chosen tour and intent should be related to the Saleor storefront's shopping basket – however, you can take any viewpoint that you can justify and that is possible to carry out on the Saleor storefront.
    Take the tour (perform the exploratory test session according to the tour idea and intent).
        Use a timer to record how long the tour lasts.
        As you explore, keep notes in the session sheet. Remember that the notes should be detailed enough for someone else to understand what you did – what general path you took on the tour.
        The notes should also address the tour intent: it should be possible to understand how the exploration fulfilled the intent.
        Also record any faults and issues you find. Faults refer to actual software code faults while issues highlight more general problematic or risky behaviour in the SUT.
    Once the tour is finished, finalise the report file and save it as a text file. The tour report file must be named "shoppingbasket-tour- <yourname>.txt".

Step 2: Session-based test management

    Get familiar with session-based test management through the lecture materials and the paper by Bach (2000).
    Prepare for the test session by copying the provided session sheet into a text editor. (session-sheet.txt)
    Perform the exploratory test session.
        The test is time-boxed to 45 minutes. Use a timer to make sure you do not exceed this time.
        As you explore, keep notes in the session sheet. Remember that the notes should be detailed enough for someone else to understand what you did – what general path you took on the tour.
        The notes should also address the session charter: it should be possible to understand how the exploration fulfilled the charter.
        Also record any faults and issues you find. Faults refer to actual software code faults while issues highlight more general problematic behaviour.
        Keep track of how much time goes to initial familiarisation and getting ideas versus actual test execution and record this in the report. Round to the nearest minute.
        Keep track of approximately how much of the time goes to the chartered test and how much is opportunistic exploration and record this in the report. You can estimate this to the nearest 10%.
    Once the test session is over, finalise the session sheet and save it. The session sheet must be named "shoppingbasket-session-sheet-<yourname>.txt".

Step 3: Analyse, reflect, and compare

Look at your testing notes from the session-based and tour-based testing runs. Reflect on what happened, what results you got, and what you experienced. You can think of the reflection as your explanation of the test runs during a debriefing with a test lead or scrum master. Write a short, reflective report that addresses the following questions:

    In broad terms, what happened during the test execution? Were there any similarities or differences between the approaches?
    What was achieved during the tests? Were there any similarities or differences between the approaches?
    What obstacles or problems did you experience using the two approaches? Were there any similarities or differences?
    What was left open or inconclusive regarding the SUT? Were there any similarities or differences in what results the two approaches gave you or what was left out?
    What thoughts and feelings were you left with after having done the two test runs using the two approaches?

The report does not have to give a final or completely exhaustive answer to the questions. It should rather demonstrate your thinking and understanding on the themes raised in the question. Focus on essential things that demonstrate what you learned about the two approaches. The answer can be 600 words at most. The answer must be submitted using the text box in this assignment.
Tips

    In the test notes, avoid disconnected glimpses and significant gaps in the general path description. Although the test notes do not have to be a precise recording of each action, it should be possible to discern what happened during the exploration and what choices you made. Some important details are, e.g., inputs, functions used, views or pages visited, and corresponding outputs, events, and behaviour of the system.
    Write as if the test notes will be compared to someone else's exploration of the same session or tour. There should be enough detail to make such a comparison possible.
    The purpose of this assignment is to get familiar with and understand different approaches to exploratory testing. You will not necessarily find any bugs or significant issues with the SUT. You can still record potential issues although you may not have strong evidence that they can occur.
    It is likely that you will have a memory effect when you do the second step, since you have already performed an exploratory test on the same SUT. This is unavoidable but also illustrates how exploratory testing contributes to learning. You can consider this in your answer if you wish, but it is not mandatory.
    You can use the storefront as a guest customer. You do not need to register an account.
    If you need to enter an email address, you can use the address example@example.com. Of course, you will never receive email sent to this address. You do not need to test receipt of any emails. You can assume that any emails generated by the SUT are correctly sent and received and that the information they contain is correct and sufficient.
    If needed in your chosen approach, you can use the developer tools of your browser to inspect the client-side code and what happens behind the scenes in the browser. For example, in Chrome, you can right-click and select Inspect.

Checklist
File shoppingbasket-tour-<yourname>.txt has the tour-based exploratory test report.

    Tour name is filled in.
    Tour summary is filled in.
    Intent is described.
    Start timestamp is filled in.
    Tester name is filled in.
    Test execution time is filled in (minutes or hours:minutes).
    Test notes describe the actions taken in sufficient detail.
    Any found faults are listed. If no faults, write None.
    Any found issues are listed. If no issues, write None.

File shoppingbasket-session-sheet-<yourname>.txt has the session-based test management report.

    Start timestamp is filled in.
    Tester name is filled in.
    Duration is short (45 min).
    Initial familiarisation is filled in (minutes).
    Test execution is filled in (minutes).
    Charter vs. opportunity is filled in and the total is 100%.
    Data files is "#N/A".
    Test notes describe the actions taken in sufficient detail.
    Any found faults are listed. If no faults, write None.
    Any found issues are listed. If no issues, write None.

Analysis and reflection is entered in the text box.

    All questions are answered.
    The analysis demonstrates your learning and your reflection on the topic.
