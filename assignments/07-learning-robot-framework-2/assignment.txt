For this assignment we will continue with the exercises from Eficode’s Robot Framework tutorial, found here https://github.com/eficode-academy/rf-katas. 

To complete this assignment, do the following:

    Download the project to your virtual machine from https://github.com/eficode-academy/rf-katas.
    Complete the steps 4-7, found in the exercises folder.
        If you want to run the provided verifications and the linting does not pass because it can’t find the setup .args file, you can in this case skip the linting by commenting out the “run_linting()” function in the corresponding setup python file.
    Submit the completed project folder to the submission field below

(Note: RobotFramework expects TAB characters in the robot files as separators, not spaces! This is a common mistake, so if you get strange errors, make sure you have TABs in the right places!)
