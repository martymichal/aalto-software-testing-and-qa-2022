*** Settings ***
Library    SeleniumLibrary
Resource    resource.robot
Test Setup    Open Browser To Login Page
Test Teardown    Close Browser

*** Keywords ***

Do Successful Login
    Login User    ${DEMO_USER_NAME}    ${DEMO_USER_PASS}

Verify That Welcome Page Is Visible
    Location Should Contain    ${URL}/welcome.html
    Page Should Contain    Welcome Page
    Title Should Be    Welcome Page

*** Test Cases ***

Welcome Page Should Be Visible After Successful Login
    Do Successful Login
    Verify That Welcome Page Is Visible

Login Form Should Be Visible After Successful Logout
    Do Successful Login
    Click Link    link=logout
    Page Should Contain TextField    id=username_field
    Page Should Contain TextField    id=password_field
    Title Should Be    Login Page
    Page Should Contain    Login Page