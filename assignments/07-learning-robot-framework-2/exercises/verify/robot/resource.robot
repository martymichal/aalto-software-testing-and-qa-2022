*** Variables ***

${URL}    http://localhost:7272
${DEMO_USER_NAME}    demo
${DEMO_USER_PASS}    mode

*** Keywords ***

Open Browser To Login Page
    Open Browser    ${URL}    browser=gc

Login User
    [Arguments]    ${name}    ${password}
    Input Text    id=username_field    ${name}
    Input Text    id=password_field    ${password}
    Submit Form    name=login_form