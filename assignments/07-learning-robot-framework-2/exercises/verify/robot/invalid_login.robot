*** Settings ***
Library    SeleniumLibrary
Resource    resource.robot
Test Setup    Open Browser To Login Page
Test Teardown    Close Browser
Test Template    Template Error Page Is Visible When Using Incorrect Credentials

*** Keywords ***

Verify That Error Page Is Visible
    Location Should Contain    ${URL}/error.html
    Page Should Contain    Error Page
    Title Should Be    Error Page

Template Error Page Is Visible When Using Incorrect Credentials
    [Arguments]    ${name}    ${password}
    Login User    ${name}    ${password}
    Verify That Error Page Is Visible

*** Test Cases ***

Empty Username Empty Password    ${EMPTY}    ${EMPTY}
Valid Username Empty Password    ${DEMO_USER_NAME}    ${EMPTY}
Empty Username Valid Password    ${EMPTY}    ${DEMO_USER_PASS}
Invalid Username Invalid Password    foo    bar
Valid Username Invalid Password    ${DEMO_USER_NAME}    thompson
Invalid Username Valid Password    ritchie    ${DEMO_USER_PASS}