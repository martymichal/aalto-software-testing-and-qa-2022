from random import choice

class Hello:
    """A simple class for generating a hello world string."""
    
    def __init__(self, name = "World", file = "names.txt"):
        self.name = name
        self.file = file
        with open(file) as namedb:
            self.__name_list = [name.strip() for name in namedb.readlines()]

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = str(value)
    
    @property
    def name_list(self):
        return self.__name_list

    def add_name(self, name):
        self.__name_list.append(name)

    def set_random_name(self):
        self.name = choice(self.__name_list)

    def check_name(self, name):
        if name in self.__name_list:
            return 'Name found in name list'
        else:
            return 'No such name in name list'

    def remove_name(self, name):
        self.__name_list.remove(name)

    def save(self):
        with open(self.file, "w") as namedb:
            for name in self.__name_list:
                namedb.write("{}\n".format(name))

    def __str__(self):
        if len(self.name) == 0:
            return "Hello!"
        else:
            return "Hello, {}!".format(self.name)
