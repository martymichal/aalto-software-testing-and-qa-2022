# Basic template for Python unit tests
# NOTE: Use Python 3

# Import the unittest module to use the Python unit test framework
import unittest

# Import the modules and/or classes that are being tested
from hello import Hello

# A test case consists of individual steps, represented as methods.
# These methods MUST start with "test". Some special methods exist
# to initialise and finalise the test case run.


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        self.hello = Hello()

    def testHelloString(self):
        """Check that the default hello world string is present."""
        assert str(self.hello) == "Hello, World!"

    def testChangeHelloString(self):
        """Check that the hello world string changes when the name is changed."""
        self.hello.name = "Planet"
        assert str(self.hello) == "Hello, Planet!"

    # Add the method testEmptyName here
    def testEmptyName(self):
        """Check that the hello world string is 'Hello!' when there's no one specific to greet"""
        self.hello.name = ""
        assert str(self.hello) == "Hello!"


# This is what gets run when Python is started on this file.
if __name__ == "__main__":
    # The unittest framework will discover all test cases in the file
    # and run the test methods in them.
    unittest.main()
