*** Settings ***
Library	     SeleniumLibrary

*** Variables ***
${URL}	      http://127.0.0.1:5000/
${BROWSER}    Chrome

*** Test Cases ***
General Greeting
	[Setup]	Open Main Page
	Check Greeting
	Reset State
	[Teardown]	Close Browser

Greeting With A Specific Name
	 [Setup]   Open Main Page
	 Submit Name
	 Check Named Greeting
	 Reset State
	 [Teardown]  Close Browser

*** Keywords ***
Open Main Page
     Open Browser     ${URL}	${BROWSER}

Reset State
      Go To	${URL}/reset

Check Greeting
      Element Text Should Be	xpath=//*[@id='greeting']	Hello, Web!

Submit Name
      Input Text	xpath=//*[@id='name']	Robot
      Click Button	xpath=//*[@id='hello']

Check Named Greeting
      Element Text Should Be	xpath=//*[@id='greeting']	Hello, Robot!