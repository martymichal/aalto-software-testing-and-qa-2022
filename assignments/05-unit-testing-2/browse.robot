*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${URL}    http://127.0.0.1:5000
${BROWSER}    Chrome

*** Test Cases ***
Main Page Greeting
    [Setup]    Open Main Page
    Check Main Page Greeting
    [Teardown]    Reset And Close Browser

Greeting With A Specific Name
    [Setup]    Open Main Page
    Submit Name
    Check Named Greeting
    [Teardown]    Reset And Close Browser

Greeting With A Random Name
    [Setup]    Open Main Page
    Click No Name Button
    Check Any Name Greeting
    [Teardown]    Reset And Close Browser

Conversation Works
    [Setup]    Open Main Page
    Submit Name
    Submit Statement
    Check For Response
    [Teardown]    Reset And Close Browser

*** Keywords ***
Open Main Page
    Open Browser    ${URL}    ${BROWSER}

Reset And Close Browser
    Go To    ${URL}/reset
    Close Browser

Submit Name
    Input Text    xpath=//*[@id='name']    Robot
    Click Button    xpath=//*[@id='hello']

Submit Statement
    Input Text    xpath=//*[@id='statement']    Is this working?
    Click Button    xpath=//*[@id='hello']

Check Main Page Greeting
    Element Text Should Be    xpath=//*[@id='greeting']    Hi! What's your name?
    Title Should Be    Hello Web

Check Named Greeting
    Element Text Should Be    xpath=//*[@id='conversation']/p    Hello, Robot!

Click No Name Button
    Click Button    xpath=//*[@id='no_name']

Check Any Name Greeting
    ${greeting}=    Get Text    xpath=//*[@id='conversation']/p
    Should Match Regexp    ${greeting}    Hello, \\w+!

Check For Response
    Element Text Should Not Be    xpath=//*[@id='conversation']/p    ${EMPTY}