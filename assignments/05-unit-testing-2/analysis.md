# CS-E4960 Software Testing and Quality Assurance D - Unit testing 2

## Why are the unit tests slow?

`setUp()` is called before every test fixture and in the set up 3 `Hello` classes are initialized. Creating a `Hello` class involves training the chat bot which is time-intensive (over 1 second).

## How to make the unit tests faster?

1. Recognize `None` as a value in `conversation_file` parameter as a value that turns off the training of the chat bot
2. Limiting the number of initialized `Hello` classes to 1
    - tests requiring a specific initialization parameter for an instance of the `Hello` class can initialize it in their body and with disabled bot training
3. Moving the initialization of `self.hello` to `setUpClass()` class method
    - there are tests changing the class instance -> use `tearDown()` for setting default values to `self.hello`

## Results of proposed changes

Execution time before changes: ~64 seconds

- Change 1. does not have an impact on the execution time
    - the test cases did not make use of the added capability
- Change 2. lowered the execution time to ~27 seconds
    - the two additional `Hello` class instances are no longer created for every test cases and in their test cases they don't invoke the learning of the chatbot
- Change 3. lowered the execution time to ~8 seconds
    - the instance of `Hello` class is only created once for all test cases
