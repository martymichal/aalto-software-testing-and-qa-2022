# A simple Hello World web application

from enum import Enum
from flask import Flask, request, redirect, url_for, render_template
from hello import Hello

class Statement:
    def __init__(self, text, speaker):
        self.text = text
        self.speaker = speaker

class Speaker(Enum):
    user = 1
    bot = 2

app = Flask(__name__)
hello = Hello("Web")
conversation = []

@app.route("/", methods=["GET", "POST"])
def helloweb():
    """Display the app front page."""

    if request.method == "POST":
        # We have received some form data

        if not request.form["name"]:
            # We didn't get a name, pick a random one
            hello.set_random_name()
        else:
            # We got a name, update our Hello object
            hello.name = request.form["name"]
            hello.add_name(request.form["name"])
            hello.save()

        # We are now ready to start a conversation
        return redirect(url_for("converse"))

    # We didn't receive any form data, just display the front page
    return render_template("helloweb.html")

@app.route("/converse", methods=["GET", "POST"])
def converse():
    """Hold a conversation."""

    if request.method == "POST":
        # We have received some form data

        if request.form["statement"]:
            statement = request.form["statement"]
            conversation.append(Statement(statement, Speaker.user))
            conversation.append(Statement(hello.get_response(statement), Speaker.bot))
        elif request.form["action"] == "quit":
            return redirect(url_for("reset"))
        else:
            conversation.append(Statement(hello.get_greeting(), Speaker.bot))
    else:
        conversation.append(Statement(hello.get_greeting(), Speaker.bot))

    # Render the page
    return render_template("converse.html", hello=hello, conversation=conversation)

@app.route("/reset")
def reset():
    """Reset the internal state."""

    global hello, conversation
    hello = Hello("Web")
    conversation = []
    return redirect(url_for('helloweb'))
