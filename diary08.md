# CS-E4960 Learning diary 08

# 1. Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

Tomi introduced us to the context of testing in a large enterprise (OP Financial Group). Financial applications are extremely complex, require immense funding. The enterprise development context is constantly changing and at least in OP they are leaning toward agile practices. After the introduction we brianstormed our approach to testing. The next topic was architecture of testing: when and how to test. There are many approaches to testing and there is no "one-to-rule-them-all" approach. Every project is unique and requires unique approach. Testability was the next discussed topic (see more info in the answer to question 2a). Test requirements followed next: using tests to match project requirements. This helps preventing misunderstanding of requirements -> (A)TDD (Automated test driven development). Kano model was the last introduced concept (see more info in the answer to question 3).

# 2. Answer ONE of the following questions:

## 2a. What did the guest lecturer mean by testability? Shortly discuss some important aspects of how it can be achieved, according to the guest lecturer and the related materials.

Degree to which a test subject is testable. This "metric" changes with the testing context. Usually the larger it is the lower the testability of the subject is. It also shows to what degree the test environment can be controlled (APIs, mocking, logs, databases,..)

Ways to achieve testability:

- checking of testability throughout development instead of tackling it at once at an advanced stage of a project development.
- not limiting the testing to a single type (e.g., just unit testing)


## 2b. What did the guest lecturer mean by test architecture? Shortly discuss some ways in which it is important for software quality and testability, according to the guest lecturer and the related materials.

---

# 3. What does the Kano model tell us about software quality and testing? Briefly discuss the most important insights from the guest lecture and the related materials.

It helps developers/testers to find required quality aspects of a software: basic requirements are crucial but "delighters" are a cherry on top.

To succeed at a market a product needs to be at the right time at the right place. This exactness requires the basic requirements/needs of the product to be fulfilled in the first place. Without those the product has no chance of succeeding. When the basic needs are achieved the "delighters" can be pursued.

"Do business, not quality" - Dr. Noriaki Kano
