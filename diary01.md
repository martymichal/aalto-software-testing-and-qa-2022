# CS-E4960 Learning diary 01

## 1. Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

- Course practicalities
- Difference between errors, faults, failures and incidents
    - errors are hard to spot in huge codebases
- Software testing and assurance of its quality is critical - several examples
  of problems caused by bugs in software in areas like space missions or banks
- Brainstormed if bug-free software is possible -> consensus is that not due to
  many reasons: time, price, complexity, shift in the direction of the sw
- Learned about concepts like **quality**, **quality assurance**, **types of
  problems** and **stages of a testing framework**
- Shared favourite software incidents

## 2. What is Software Quality according to Kan? What do you think this definition mean in terms of achieving software quality?

Combination of conformance to requirements (starting with bug-less
implementations; made of defect rate and reliability), customer satisfaction and
satisfaction with specific attributes of the software ("quality parameters";
e.g., performance, maintainability, capability,..).

A definition of software quality provides concrete insight into what aspects it is
made of thus making the creators able to focus on concrete goals.

## 3. What is Software Quality Assurance according to Mistrik et al.? What do you think is the role of a software quality model in software QA?

```
Organizational quality guide independent of a particular project that includes
set of standards regulations, best practices and SW tools...
```

A software quality model defines a set of characteristics that can be used to
properly utilize software QA.
