# CS-E4960 Learning diary 02

# Summarise the main points of the previous lecture. What new things did you learn and what questions were left open?

- clarification of naming of terms: error (mistake), fault (defect), failure, incident
    - it is very valuable to know exactly what to focus on and what a certain "defect" exactly is
    - higher fault rate is better than high failure rate
- introduction of ISO standard defining term "software quality"
    - centred on stakeholders instead of technical excellence
- brainstormed questions regarding:
    - definition of sw quality in a dev project to make effective and manageable
    - definition of quality so that it could be used in a real project
- deep dive to the definition of "quality" and its parts (models, criteria)
    - types of focus (different view of quality): consumer, producer, support
    - sw quality is not just about the code
    - different characteristics models: Boehm, McCall, ISO-9126,..)
- deep dive into the ISO/IEC 25010 (SQuaRE) standard (
- importance of stakeholder perspectives (primary > secondary user)
- difference between requirements and exact steps to meet them

# Describe the general Software Quality Assurance model by which quality can be made concrete and measurable. (According to Mistrik and the lecture.)

A model is made several factors/characteristics of which every can be disassembled into measurable units by clearly defining their meaning and careful application to the software project needs.

# Pick two quality characteristics from ISO/IEC 25010 (SQuaRE). Explain them according to the definitions given in the standard. How would you assure quality for them?

- satisfaction
    - level of how users find the sw to be feature-complete, easy-to-use and its trustworthiness (does it break?)
    - user testing during development, user surveys/studies during maintenance

- efficiency
    - ratio between expended energy and completeness + accuracy of the desired action
    - regression tests (new changes don't slow down the execution), unit testing
